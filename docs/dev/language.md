# 多语言支持

我们使用`Flask-Babel`来实现多语言支持。

具体请参照[Flask-Babel说明文档](https://pythonhosted.org/Flask-Babel/)

## 生成/重新生成待翻译文本
注意应在项目根目录下执行。
```bash
pybabel extract -F web/babel.cfg -o web/messages.pot web
pybabel update -i web/messages.pot -d web/translations
```

## 应用新翻译
```bash
pybabel compile -d web/translations
```

## ~~添加新语言~~
~~这玩意应该一般不会用到吧~~  
~~`pybabel init -i web/messages.pot -d web/translations -l <name>`~~