# 开发人员文档

+ [架构](architecture.md)
+ [API](api.md)
+ [多语言](language.md)

## 给国内用户的提示

如果pip安装速度过慢，参照[https://mirrors.tuna.tsinghua.edu.cn/help/pypi/](https://mirrors.tuna.tsinghua.edu.cn/help/pypi/)将pip换为清华源。

## 搭建开发环境

**注意以下命令都应该在项目的根目录执行。**

我们使用~~由Flask官方推荐的~~virtualenv搭建运行时（虚拟）环境。

使用如下指令安装virtualenv并创建环境。
```bash
pip install virtualenv
virtualenv --no-site-packages venv
```

创建好环境后使用如下指令进入该环境。
```bash
source venv/bin/activate
```

您可以直接退出或关闭终端来退出虚拟环境，或者使用：
```bash
deactivate
```