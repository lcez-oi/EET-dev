# API文档

## 页面相关

### 设置语言

方法: `GET`
地址: `/api/set_lang/<string:lang_name>`

可用的`lang_name`: `zh_Hans` `en`

成功返回信息:
```json
{
    "status": 200,
    "error": ""
}
```
失败（无此语言）返回信息:
```json
{
    "status": 404,
    "error": "No such language"
}
```

## 前后端通信相关

### 评测

#### 获取评测任务队列

方法: `GET`
地址: `/api/jobs`

返回信息:
```json
{
    "count": <int>,
    "list": [
        <string>
    ]
}
```

`count` 为一非负整数, 表示等待评测的任务数量.  
`list` 为一数组, 保证其中包含 `count` 个元素, 且每个元素对应一个任务. 每个元素都是一个长为 $10$ 的字符串，仅包含数字、大小写字母, 称为该评测任务的 `id`.

#### 获取任务详情

方法: `GET`
地址: `/api/job/<int:job_id>`

