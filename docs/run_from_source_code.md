# 从源码运行EET

+ 首先请确保您的电脑上安装了`python3`且`pip`指令可用（若`pip`指令但`python`指令可用，您可以使用`python -m pip`来代替下文中的`pip`
+ 首先您需要安装依赖。
```bash
pip install -r requirements.txt
```
对于中国用户：如果pip安装速度过慢，您可以使用:
```bash
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r requirements.txt
```
如果报错，可能是因为权限问题，您可以尝试`sudo pip install -r requirements.txt`

+ 然后您只需要运行`EET.py`
```bash
python EET.py
```
注意`python`指令启动的是`python3`。您也可以使用`python3`指令。
+ 然后您可以在您的浏览器中打开[http://localhost:8091/](http://localhost:8091/)  
