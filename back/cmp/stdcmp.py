import sys

argc = sys.argv

print("EET stdcmp version 0.1.0")

if len(argc) != 3:
    print("[Error] Argument amount error.")
    sys.exit(4) # return 1024 then

try:
    with open(argc[1], 'r') as lfile:
        lstr = lfile.read().split()
except FileNotFoundError:
    print("[Error] No such file or dictionary: \"{}\".".format(argc[1]))
    sys.exit(3) # return 768 then

try:
    with open(argc[2], "r") as rfile:
        rstr = rfile.read().split()
except FileNotFoundError:
    print("[Error] No such file or dictionary: \"{}\".".format(argc[2]))
    sys.exit(3) # return 768 then

if len(lstr) != len(rstr):
    print("[WA] Length is not equal.")
    sys.exit(2) # return 512 then

for x in range(1, len(lstr) + 1, 1):
    if lstr[x] != rstr[x]:
        print("[WA] read {} from {}, but {} from {}.".format(lstr[x], argc[1], rstr[x], argc[2]))
        sys.exit(1) # return 256 then

print("[AC] No differences.")
sys.exit(0)