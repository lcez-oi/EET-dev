import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system


class Compile_res():
    '''
    To store the result of compile process
    '''

    def __init__(self, return_code: int, extra_msg: str, time_consume: float):
        '''
        Just need return code and msgs
        '''
        self.return_code = return_code
        self.extra_msg = extra_msg
        self.time_consume = time_consume

    def if_CE(self):
        return self.return_code is not 0

    def __str__(self):
        res = "return code: {}\n".format(self.return_code)
        res = res + "time consume: \n {} seconds. \n".format(self.time_consume)
        if len(self.extra_msg) is 0:
            res = res + "No extra message.\n"
        else:
            res = res + "Extra message:\n{}\n".format(self.extra_msg)
        return res

    def __format__(self):
        return self.__str__()


class Compiler():
    '''
    This is EET's compiler.
    For convenience I put it here. \n
    What you need to confirm:
    1. what compile generator as a string
    2. compile_args as a list filled with strings
    3. compile file as full path(string)
    4. goal file as full path(string)
    5. system if you do not have platform module
    '''

    def __init__(self, compile_file: str, generater: str, compile_args: list, goal_file=None):
        '''
        To origin the compiler,
        I have to restress that, 
        what you need to confirm: \n
        1. what compile generator as a string
        2. compile_args as a list filled with strings
        3. compile file as full path(string)
        4. goal file if you needed as full path(string)
        5. system if you do not have platform module
        '''
        self.compile_file = compile_file
        self.goal_file = goal_file
        self.generater = generater
        self.compile_args = compile_args

    def compile_cmd(self):
        '''
        To return compile command if you really need it.
        '''
        Args = ""
        for arg in self.compile_args:
            Args = Args + " " + str(arg)
        cmd = "{} {} {} {}".format(
            self.generater, self.compile_file, Args, self.goal_file
        )
        return cmd

    def compile_it(self, cmd=""):
        '''
        To make compile process and return a compile_res class. \n
        Let cmd as "" if you do not want to make the cmd yourself.
        '''
        if cmd == "":
            cmd = self.compile_cmd()
        p = sp.Popen(
            cmd, shell=True, cwd=os.path.dirname(self.compile_file), stdout=sp.PIPE, stderr=sp.PIPE
        )
        ltime = time.time()
        while p.poll() is False:
            time.sleep(0.05)
        rtime = time.time()
        rmsg, lmsg = p.communicate()
        return Compile_res(p.returncode, (str(lmsg) + ' ' + str(rmsg)), rtime - ltime)


class Runner_cfg():
    '''
    set the limits of the running program.
    memory limit is in Bytes(1/2^20 MB).
    time limit is in second(1 s).
    dir is where do you run the program.
    '''
    def __init__(self, time_limit: float, memory_limit: float, dir_work: str, stdio=False, fin=None, fout=None):
        '''
        If run as stdio,
        You should let stdio = True first.\n
        If not, You should provide file input as 
        open('<full input file path>', 'r') \n
        and file output as
        open('full output file path', 'w+').
        '''
        self.time_limit = time_limit
        self.memory_limit = memory_limit
        self.stdio = stdio
        self.fin = fin
        self.fout = fout


class Runner_res():
    '''
    Strage of a runner's once run
    '''

    def __init__(self, safe: bool, time_consume=0.0, memory_consume=0.0):
        '''
        Initaization
        '''
        self.safe = safe
        self.time_consume = time_consume
        self.memory_consume = memory_consume

    def __str__(self):
        '''
        Show the res in a string!
        '''
        res = "Is safe: "
        if self.safe:
            res = res + "Safe. \n"
        else:
            res = res + "Unsafe. \n"
        res = res + \
            "Time consume: {} seconds. \n".format(round(self.time_consume, 5))
        res = res + \
            "Memory consume: {} KB. \n".format(self.memory_consume / (1024))
        return res

    def __format__(self):
        return self.__str__()


class Runner(Runner_cfg):
    def __init__(self, start_cmd: str, time_limit: float, memory_limit: int, dir_work: str, stdio=False, fin=None, fout=None):
        super().__init__(time_limit, memory_limit, dir_work, stdio, fin, fout)
        self.start_cmd = start_cmd

    def run_code(self):
        if self.stdio:
            max_rss = 0
            p = sp.Popen(
                self.start_cmd, shell=True, cwd=self.dir_work,
                stdin=self.fin, stdout=self.fout, stderr=sp.PIPE
            )
            ltime = time.time()
            pid = p.pid
            monitor = psutil.Process(pid)
            while True:
                if psutil.pid_exists(pid) is False and p.returncode is not 0:
                    return runner_res(False)
                memory_info = monitor.memory_info()
                now_rss = memory_info[0]
                # now_t_consume = time.time() - ltime
                max_rss = max(max_rss, now_rss)
                if p.poll():
                    return runner_res(True, time.time() - ltime, max_rss)
                if time.time() - ltime > self.time_limit:
                    monitor.terminate()
                    return runner_res(True, self.time_limit, max_rss)
                if max_rss > self.memory_limit:
                    monitor.terminate()
                    return runner_res(True, time.time() - ltime, self.memory_limit)
        else:
            max_rss = 0
            p = sp.Popen(
                self.start_cmd, cwd=self.dir_work,
                stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE
            )
            ltime = time.time()
            pid = p.pid
            monitor = psutil.Process(pid)
            while True:
                if p.poll() or psutil.pid_exists(pid) is False:
                    if p.returncode is 0:
                        return runner_res(True, time.time()-ltime, max_rss)
                    else:
                        return runner_res(False, time.time()-ltime, max_rss)
                memory_info = monitor.memory_info()
                now_rss = memory_info[0]
                # now_t_consume = time.time() - ltime
                max_rss = max(max_rss, now_rss)
                if time.time() - ltime > self.time_limit:
                    monitor.terminate()
                    return runner_res(True, self.time_limit, max_rss)
                if max_rss > self.memory_limit:
                    monitor.terminate()
                    return Runner_res(True, time.time() - ltime, self.memory_limit)
