import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system


class Data_set():
    '''
    A group of datas for one problem.\n
    It can check how many datas automatically and match them.\n
    You can give it the data's dir path and the input file shuffix, output file shuffix
    to check it.
    '''

    def __init__(self, data_dir: str, input_shuffix: str = ".in", output_shuffix: str = ".out", datas: list = []):
        '''
        1. data_dir for the dir path of the problem data set.
        2. input_shuffix for the input file shuffix format, like ".in"
        3. output_shuffix for the output file shuffix format, like ".out" or ".ans"
        '''
        self.data_dir = data_dir
        self.input_shuffix = input_shuffix
        self.output_shuffix = output_shuffix
        self.datas = datas

    def flush_data_set(self):
        input_datas = []
        output_datas = []
        for file_name in os.listdir(self.data_dir):
            if not os.path.isfile(file_name):
                pass
            shuffix = os.path.splitext(file_name)[1]
            if shuffix is self.input_shuffix:
                input_datas.append((self.data_dir, file_name))
            elif shuffix is self.output_shuffix:
                input_datas.append((self.data_dir, file_name))
        for x in range(len(input_datas)):  # match the input files and output files
            prefix_x = input_datas[x][0]
            for y in range(len(output_datas)):
                prefix_y = output_datas[y][0]
                if prefix_x == prefix_y:
                    self.datas.append(
                        (os.path.join(prefix_x + input_datas[x][1], self.data_dir),
                         os.path.join(prefix_y + output_datas[y][1], self.data_dir)))
        del input_datas, output_datas
