import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system


class Competitor_set():
    def flush_competitors(self):
        l = os.listdir(self.Dir)
        for item in l:
            if os.path.isdir(item):
                self.competitors.append(os.path.join(self.Dir, item))

    def __init__(self, Dir: str, competitors: list = []):
        self.Dir = Dir
        self.competitors = competitors
        self.flush_competitors()
