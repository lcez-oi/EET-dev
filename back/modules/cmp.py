import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system


class Compare_res():
    def __init__(self, return_code, extra_msg):
        self.return_code = return_code
        self.extra_msg = extra_msg

    def if_get_score(self):
        return self.return_code is 0

    def __str__(self):
        if self.return_code is 0:
            res = "Status: Accepted.\n"
        else:
            res = "Status: Unaccepted.\n"
        if len(self.extra_msg) is 0:
            res = res + "No extra message.\n"
        else:
            res = res + self.extra_msg
        return res

    def __format__(self):
        return self.__str__()


class Comparer():
    def __init__(self, compare_method: str, output_file: str, answer_file: str):
        self.compare_method = compare_method
        self.output_file = output_file
        self.answer_file = answer_file

    def cmp_cmd(self):
        return self.compare_method + ' ' + \
            self.output_file + ' ' + \
            self.answer_file

    def compare_it(self, cmd=""):
        if cmd == "":
            cmd = self.cmp_cmd()
        p = sp.Popen(cmd, cwd=os.path.dirname(self.output_file),
                     stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
        while True:
            if p.poll():
                break
            time.sleep(0.5)
        out, err = p.communicate()
        return Compare_res(p.return_code, str(out) + ' ' + str(err))

