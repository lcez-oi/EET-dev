import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system
from compile_run import Compiler
from compile_run import Compile_res


class Spj_compiler(Compiler):
    def __init__(self, compile_file: str, generater: str, compile_args: list, testlib_path: str, goal_file: str = "", System=system()):
        super().__init__(compile_file, generater, compile_args, goal_file, System)
        self.testlib_path = testlib_path

    def compile_it(self, cmd: str = ""):
        if self.System == "Windows":
            os.system("copy {} {}".format(
                self.testlib_path, os.path.dirname(self.compile_file)))
        else:
            os.system("cp {} {}".format(
                self.testlib_path, os.path.dirname(self.compile_file)))
        if cmd == "":
            cmd = self.compile_cmd()
        p = sp.Popen(cmd, shell=True,
                     cwd=os.path.dirname(self.compile_file), stdout=sp.PIPE, stderr=sp.PIPE)
        while p.poll() is False:
            time.sleep(0.5)
        rmsg, lmsg = p.communicate()
        return Compile_res(p.returncode, str(lmsg) + str(rmsg))


class Spj_res():
    def __init__(self, return_code: int, score: float, extra_msg: str, report: str):
        self.return_code = return_code
        self.score = score
        self.extra_msg = extra_msg
        self.report = report

    def __str__(self):
        if self.return_code is 0:
            res = "No error during special judge.\n"
        else:
            res = "Special judge failed!\n"
        res = res + "Extra message: \n" + self.extra_msg + '\n'
        res = res + "Report message:\n" + self.report + '\n'
        return res

    def __format__(self):
        return self.__str__()


class Spj_runner():
    '''
    Special runner class. \n
    Give input file(answer's input), output file(competitor's output) \n
    answer file(...), spj's args like ["-o2", "-o"]... \n
    No time and memory limits. 
    '''

    def __init__(self, spjer: str, input_file: str, output_file: str,
                 answer_file: str, single_score: float, score_file: str,
                 report_file: str, spjpos: str):
        '''
        Special judger class. \n
        Give input file(answer's input), output file(competitor's output) \n
        answer file(...), spj's args like ["-o2", "-o"]... \n
        No time and memory limits. 
        All string is a absolute path of the file!
        '''
        self.spjer = spjer
        self.spjpos = spjpos
        self.input_file = input_file
        self.output_file = output_file
        self.answer_file = answer_file
        self.score_file = score_file
        self.single_score = single_score
        self.report_file = report_file

    def spjcmd(self):
        '''
        equal to use "%i %o %a %r %sc %sf"
        '''
        System = system()
        if System is "Windows":
            res = "start "
        else:
            res = ""
        res = res + "{} {} {} {} {} {}".format(
            self.input_file, self.output_file,
            self.answer_file, self.report_file,
            self.single_score, self.score_file)
        return res

    def sp_spjcmd(self, spcmd: str):
        if spcmd.find("%i") is not -1:
            spcmd.replace("%i", self.input_file)
        if spcmd.find("%o") is not -1:
            spcmd.replace("%o", self.output_file)
        if spcmd.find("%a") is not -1:
            spcmd.replace("%a", self.answer_file)
        if spcmd.find("%sc") is not -1:
            spcmd.replace("%sc", self.single_score.__str__())
        if spcmd.find("%sf") is not -1:
            spcmd.replace("sf", self.score_file)
        if spcmd.find("%r") is not -1:
            spcmd.replace("%r", self.report_file)
        return spcmd

    def spj_run(self, cmd=""):
        if cmd == "":
            cmd = self.spjcmd()
        else:
            cmd = self.sp_spjcmd(cmd)
        p = sp.Popen(cmd, cwd=self.spjpos,
                     stdin=sp.PIPE, stdout=sp.PIPE, stderr=sp.PIPE)
        while p.poll() is False:
            time.sleep(0.5)
        out, err = p.communicate()
        report = open(self.report_file, "r").read()
        try:
            score = open(self.score_file, "r").read().__float__()
        except FileNotFoundError:
            return Spj_res(-1, 0, "Failed to load score", "Sorry.")
        except ValueError:
            return Spj_res(-1, 0, "Failed to convert score", "Sorry.")
        return Spj_res(p.returncode, score, str(out) + ' ' + str(err), report)
