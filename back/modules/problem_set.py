import os
import sys
import time
import json
import psutil
import subprocess as sp
from platform import system
import data_set as ds


class Problem_set():
    def flush_problems(self):
        l = os.listdir(self.exam_dir)
        for item in l:
            if os.path.isdir(item):
                self.problems.append(ds.Data_set(
                    os.path.join(self.exam_dir, item), self.input_shuffix, self.output_shuffix))

    def __init__(self, exam_dir: str, input_shuffix: str = ".in", output_shuffix: str = ".out", problems=[]):
        self.exam_dir = exam_dir
        self.problems = problems
        self.flush_problems()
