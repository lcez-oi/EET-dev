#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from web import app, conf, socketio


if __name__ == '__main__':
    serv_conf = conf['web']['server']
    socketio.run(app, host=serv_conf['host'], port=serv_conf['port'], debug=serv_conf['debug'])
