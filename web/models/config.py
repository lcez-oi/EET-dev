import json


class Config(dict):

    def __init__(self, path):
        self.path = path
        with open(path, 'r') as f:
            data = json.loads(f.read())
            for key, value in data.items():
                self[key] = value

    def save(self):
        with open(self.path, 'w') as f:
            f.write(json.dumps(self, indent=4))
