from flask import render_template
from flask_babel import gettext as _

from web import app


@app.route('/preferences/')
def preferences():
    return render_template('preferences.html', page='preferences')
