import json

from web import app, babel, conf


def get_conf():
    return conf
app.add_template_global(get_conf, 'get_conf')

def get_lang():
    return map(lambda x: {'id': x.__str__(), 'display_name': x.display_name}, babel.list_translations())
app.add_template_global(get_lang, 'get_lang')

from .favicon import *
from .index import *
from .preferences import *
