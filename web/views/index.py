from flask import render_template
from flask_babel import gettext as _

from web import app


@app.route('/')
def index():
    return render_template('index.html', page='index')
