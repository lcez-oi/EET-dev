from web import app
from flask import url_for, redirect


@app.route('/favicon.ico')
def favicon():
    return redirect(url_for('static', filename='favicon.ico'))
