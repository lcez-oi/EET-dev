from web import babel, conf


@babel.localeselector
def lang():
    return conf['web']['display']['language']
