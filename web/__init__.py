from flask import Flask
from flask_babel import Babel
from flask_restful import Api
from flask_socketio import SocketIO
from pathlib import Path

from web.glob import CONFIGURATION_PATH
from web.models import Config


app = Flask(__name__)
api = Api(app)
babel = Babel(app)
socketio = SocketIO(app)

conf = Config(CONFIGURATION_PATH)

app.config['SECRET_KEY'] = conf['web']['server']['secret_key']

import web.api
import web.controller
import web.models
import web.views
