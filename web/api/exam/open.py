from flask import g
from flask_restful import Resource, reqparse
from json import JSONDecodeError
from pathlib import Path

from web import api, app, conf
from web.models import Exam


class open_exam(Resource):
    def __init__(self):
        super().__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', required=True)
    def get(self):
        args = self.parser.parse_args()
        path = app.root_path / conf['general']['filenames']['exams'] / args['name'] / conf['general']['filenames']['exam_data']
        if path.exists():
            try:
                exam = Exam(path)
            except JSONDecodeError:
                return {'message': {'name': 'Invalid configuration file'}}, 400
            except:
                return {'message': {'name': 'Unknown error'}}, 500
            setattr(g, 'exam', exam)
            return {}, 200
        else:
            return {'message': {'name': 'NO such exam found in \'exams\' directory'}}, 405
api.add_resource(open_exam, '/api/exam/open/')
