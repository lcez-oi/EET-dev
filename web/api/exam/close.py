from flask import g
from flask_restful import Resource, reqparse

from web import api


class close_exam(Resource):
    def __init__(self):
        super().__init__()
    def get(self):
        if hasattr(g, 'exam'):
            getattr(g, 'exam').save()
            delattr(g, 'exam')
            return {}, 200
        else:
            return {'message': 'NO open exam currently'}, 404
api.add_resource(close_exam, '/api/exam/close')
