from flask_restful import Resource, reqparse, inputs

from web import api, conf


class set_host(Resource):
    def __init__(self):
        super().__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('host', required=True)
    def get(self):
        args = self.parser.parse_args()
        conf['web']['server']['host'] = args['host']
        conf.save()
        return {}, 200
api.add_resource(set_host, '/api/set/adv/host/')

class set_port(Resource):
    def __init__(self):
        super().__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('port', type=int, required=True)
    def get(self):
        args = self.parser.parse_args()
        port = args['port']
        if port < 1024 or port > 65535:
            return {'message': {'port': 'NOT in range [1024, 65535]'}}, 416
        conf['web']['server']['port'] = port
        conf.save()
        return {}, 200
api.add_resource(set_port, '/api/set/adv/port/')

class set_debug(Resource):
    def __init__(self):
        super().__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('debug', type=inputs.boolean, required=True)
    def get(self):
        args = self.parser.parse_args()
        conf['web']['server']['debug'] = args['debug']
        conf.save()
        return {}, 200
api.add_resource(set_debug, '/api/set/adv/debug/')
