from flask_babel import refresh, Locale
from flask_restful import Resource, reqparse

from web import api, babel, conf


class set_lang(Resource):
    def __init__(self):
        super().__init__()
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('lang', required=True)
    def get(self):
        args = self.parser.parse_args()
        lang = args['lang']
        if Locale.parse(lang) not in babel.list_translations():
            return {'message': {'lang': 'NO such language'}}, 404
        conf['web']['display']['language'] = lang
        conf.save()
        refresh()
        return {}, 200
api.add_resource(set_lang, '/api/set/lang/')
